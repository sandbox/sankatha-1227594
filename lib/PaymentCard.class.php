<?php
/**
 * The class which sets up the
 * payment card data
 */
class PaymentCard {
  private $cardType;
  private $cardNumber;
  private $cardHolderName;
  private $cardCvc;
  private $cardIssueNo;
  private $expiryDate = array(
    'month' =>'' ,
    'year' => ''
  );
  private $cardHolderAddress = array(
    'first_name' => '',
    'last_name' => '',
    'street' => '',
    'postal_code' => '',
    'country_code' => ''
  );
  private $cardHolderTelephone;

  /**
   * Set card holder name
   * @param $cardHolderName
   * @return void
   */
  public function setCardHolderName($cardHolderName) {
    $this->cardHolderName = $cardHolderName;
  }

  /**
   * Get the card holder name
   * @return string
   */
  public function getCardHolderName() {
    return $this->cardHolderName;
  }

  /**
   * Set card number
   * @param $cardNumber
   * @return void
   */
  public function setCardNumber($cardNumber) {
    $this->cardNumber = $cardNumber;
  }

  /**
   * Get the card number
   * @return int
   */
  public function getCardNumber() {
    return $this->cardNumber;
  }

  /**
   * Set the card type
   * @param $cardType
   * @return void
   */
  public function setCardType($cardType) {
    $this->cardType = $cardType;
  }

  /**
   * Get the card type
   * @return string
   */
  public function getCardType() {
    return $this->cardType;
  }

/**
 * Set expiry date
 * @param $month
 * @param $year
 * @return void
 */
  public function setExpiryDate($month, $year) {
    $this->expiryDate = array(
      'month' => $month,
      'year'  => $year
    );
  }

  /**
   * Get the expiry date
   * @return array
   */
  public function getExpiryDate() {
    return $this->expiryDate;
  }

  /**
   * Set card holder address
   * @param $street
   * @param $postal_code
   * @param $country_code
   * @return void
   */
  public function setCardHolderAddress($first_name, $last_name, $street, $postal_code, $country_code) {
    $this->cardHolderAddress = array(
      'first_name' => $first_name,
      'last_name' => $last_name,
      'street' => $street,
      'postal_code' => $postal_code,
      'country_code' => $country_code
    );
  }

  /**
   * Return card holder address
   * @return array
   */
  public function getCardHolderAddress() {
    return $this->cardHolderAddress;
  }

  /**
   * Set card holder number
   * @param $cardHolderTelephone
   * @return void
   */
  public function setCardHolderTelephone($cardHolderTelephone) {
    $this->cardHolderTelephone = $cardHolderTelephone;
  }

  /**
   * Get card holder number
   * @return int
   */
  public function getCardHolderTelephone() {
    return $this->cardHolderTelephone;
  }

  /**
   * Set card CVC
   * @param $cardCvc
   * @return void
   */
  public function setCardCvc($cardCvc) {
    $this->cardCvc = $cardCvc;
  }

  /**
   * Get card CVC
   * @return int
   */
  public function getCardCvc() {
    return $this->cardCvc;
  }

  /**
   * Set the issue number
   * @param $cardIssueNo
   * @return void
   */
  public function setCardIssueNo($cardIssueNo) {
    $this->cardIssueNo = $cardIssueNo;
  }

  /**
   * Get the issue number
   * @return string
   */
  public function getCardIssueNo() {
    return $this->cardIssueNo;
  }
}
 
