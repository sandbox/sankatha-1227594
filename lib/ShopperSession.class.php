<?php
/**
 * Class to pass over the shopper session details
 */
 
class ShopperSession {
  private $userid;
  private $shopperIP;
  // Should be always $_SERVER['HTTP_ACCEPT']
  private $shopperBrowserAcceptHeader;
  // Should be always $_SERVER['HTTP_USER_AGENT']
  private $shopperBrowserUserAgent;

  /**
   * Set the shopper IP address
   * @param $shopperIP
   * @return void
   */
  public function setShopperIP($shopperIP) {
    $this->shopperIP = $shopperIP;
  }

  /**
   * Return the shopper IP address
   * @return string
   */
  public function getShopperIP() {
    return $this->shopperIP;
  }

  /**
   * Set the User ID for the order
   * @param $userid
   * @return void
   */
  public function setUserid($userid) {
    $this->userid = $userid;
  }

  /**
   * Get the User ID for the order
   * @return int
   */
  public function getUserid() {
    return $this->userid;
  }

  /**
   * Set the shopper browser HTTP_ACCEPT
   * @param $shopperBrowserAcceptHeader
   * @return void
   */
  public function setShopperBrowserAcceptHeader($shopperBrowserAcceptHeader) {
    $this->shopperBrowserAcceptHeader = $shopperBrowserAcceptHeader;
  }

  /**
   * Get the shopper browser HTTP_ACCEPT
   * @return
   */
  public function getShopperBrowserAcceptHeader() {
    return $this->shopperBrowserAcceptHeader;
  }

  /**
   * Set the shopper browser HTTP_USER_AGENT
   * @param $shopperBrowserUserAgent
   * @return void
   */
  public function setShopperBrowserUserAgent($shopperBrowserUserAgent) {
    $this->shopperBrowserUserAgent = $shopperBrowserUserAgent;
  }

  /**
   * Get the shopper browser HTTP_USER_AGENT
   * @return
   */
  public function getShopperBrowserUserAgent() {
    return $this->shopperBrowserUserAgent;
  }
}
