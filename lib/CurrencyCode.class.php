<?php
/**
 * The supported currency codes
 * in Worldpay
 */
 
final class CurrencyCode {
  // The list of ISO currency codes supported by the Worldpay Gateway
  // Nuevo Argentine Peso
  const ARS = 'ARS';
  // Australian Dollar
  const AUD = 'AUD';
  // Brazilian Real
  const BRL = 'BRL';
  // Canadian Dollar
  const CAD = 'CAD';
  // Swiss Franc
  const CHF = 'CHF';
  // Chilean Peso
  const CLP = 'CLP';
  // Yuan Renminbi
  const CNY = 'CNY';
  // Colombian Peso
  const COP = 'COP';
  // Czech Koruna
  const CZK = 'CZK';
  // Danish Krone
  const DKK = 'DKK';
  // Euro
  const EUR = 'EUR';
  // Pound Sterling
  const GBP = 'GBP';
  // Hong Kong Dollar
  const HKD = 'HKD';
  // Hungarian Forint
  const HUF = 'HUF';
  // Indonesian Rupiah
  const IDR = 'IDR';
  // Iceland Krona
  const ISK = 'ISK';
  // Japanese Yen
  const JPY = 'JPY';
  // Kenyan Shilling
  const KES = 'KES';
  // South-Korean Won
  const KRW = 'KRW';
  // Mexican Peso
  const MXP = 'MXP';
  // Malaysian Ringgit
  const MYR = 'MYR';
  // Norwegian Krone
  const NOK = 'NOK';
  // New Zealand Dollar
  const NZD = 'NZD';
  // Philippine Peso
  const PHP = 'PHP';
  // New Polish Zloty
  const PLN = 'PLN';
  // Portugese Escudo
  const PTE = 'PTE';
  // Swedish Krone
  const SEK = 'SEK';
  // Singapore Dollar
  const SGD = 'SGD';
  // Slovak Koruna
  const SKK = 'SKK';
  // Thai Baht
  const THB = 'THB';
  // New Taiwan Dollar
  const TWD = 'TWD';
  // US Dollars
  const USD = 'USD';
  // Vietnamese New Dong
  const VND = 'VND';
  // South African Rand
  const ZAR = 'ZAR';

  /**
   * Check the currency code and returns true if the code is
   * supported by the Worldpay gateway
   * @param $currencyCode
   * @return boolean
   */
  public function isSupportedCurrencyCode($currencyCode) {
    // Get the supported ISO Currency codes
    $supportedISOCurrencyCodes = $this->getSupportedISOCurrencyCodes();
    if (array_key_exists(trim($currencyCode), $supportedISOCurrencyCodes)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get all the supported ISO currency codes in Worldpay
   * @return array
   */
  public function getSupportedISOCurrencyCodes() {
    $reflection = new ReflectionClass('CurrencyCode');
    $supportedISOCurrencyCodes = $reflection->getConstants();

    return $supportedISOCurrencyCodes;
  }
}