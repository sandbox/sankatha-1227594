<?php
/**
 * This is to simulate some enumeration
 * to make sure that we use the correct card
 * type
 */
 
final class PaymentCardType {
  const Amex = 'AMEX-SSL';
  const Diners = 'DINERS-SSL';
  const JCB = 'JCB-SSL';
  const Master = 'ECMC-SSL';
  const Maestro = 'MAESTRO-SSL';
  const Solo = 'SOLO_GB-SSL';
  const Visa = 'VISA-SSL';
  const Laser = 'LASER-SSL';

  // The module does not support German payment method at the moment because it
  // required shoppers bank details to be entered. Will look forward to work
  // on this if the community needs this as a feature
  // const ELV = 'ELV';

  /**
   * Check the Payment Card type and returns true if the card type is
   * supported by the Worldpay gateway
   * @param $cardType
   * @return boolean
   */
  public function isSupportedCardType($cardType) {
    // Card Type
    $cardType = trim($cardType);

    // Human readable card type
    if (preg_match('/American/i', $cardType) || preg_match('/Amex/i', $cardType)) {
      $cardType = 'Amex';
    }
    elseif (preg_match('/Visa/i', $cardType)) {
      $cardType = 'Visa';
    }
    elseif (preg_match('/Master/i', $cardType)) {
      $cardType = 'Master';
    }
    elseif (preg_match('/Maestro/i', $cardType)) {
      $cardType = 'Maestro';
    }
    elseif (preg_match('/Diners/i', $cardType)) {
      $cardType = 'Diners';
    }
    elseif (preg_match('/JCB/i', $cardType)) {
      $cardType = 'JCB';
    }
    elseif (preg_match('/Solo/i', $cardType)) {
      $cardType = 'Solo';
    }
    elseif (preg_match('/Laser/i', $cardType)) {
      $cardType = 'Laser';
    }

    // Get the supported card types
    $supportedCardTypes = $this->getSupportedCardTypes();
    // Check if the card type is supported
    if (array_key_exists($cardType, $supportedCardTypes)) {
      $reflection = new ReflectionClass('PaymentCardType');
      return $reflection->getConstant($cardType);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Return all the supported Payment card types
   * @return array
   */
  public function getSupportedCardTypes() {
    $reflection = new ReflectionClass('PaymentCardType');
    $supportedCardTypes = $reflection->getConstants();

    return $supportedCardTypes;
  }
}