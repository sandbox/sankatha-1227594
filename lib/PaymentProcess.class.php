<?php
/**
 * The class that process the payment
 * against the Worldpay Gateway
 */
require_once 'Order.class.php';
require_once 'PaymentCard.class.php';
require_once 'PaymentCardType.class.php';
require_once 'ShopperSession.class.php';

class PaymentProcess {
  private $order;
  private $paymentCard;
  private $shopperEmail;
  private $shopperSession;

  // Payment gateway service endpoints
  const TEST_GATEWAY_ENDPOINT = 'https://secure-test.wp3.rbsworldpay.com/jsp/merchant/xml/paymentService.jsp';
  const LIVE_GATEWAY_ENDPOINT = 'https://secure.wp3.rbsworldpay.com/jsp/merchant/xml/paymentService.jsp';

  /**
   * Set the order for payment processing and send the status
   * if it is passed or failed
   * @param $order_details
   * @return array
   */
  public function initiateOrderForPaymentProcessing($order_details) {
    // Create a new order object to process the payment
    $this->order = new Order();
    
    // *********************** SETTING ORDER DETAILS **************************************
    // FIX ME: Get the currency code from the settings
    // Set the currency code
    $this->order->setCurrencyCode('GBP');

    // Set the order total without fraction points. We set the fraction points
    // on the XML order request separately
    if (isset($order_details->order_total)) {
      $this->order->setAmount($order_details->order_total * 100);
    }
    else {
      throw new Exception('No order total has been set');
    }
    
    // Set the order id
    if (isset($order_details->order_id)) {
      $this->order->setOrderCode($order_details->order_id);
      $this->order->setOrderDescription(t('Order ID : ' . $order_details->order_id));
    }
    else {
      throw new Exception('No order id has been set');
    }

    // Set the order details
    if (isset($order_details->products)) {
      $ordered_products = array();
      $table_headers = array(
        t('Code'),
        t('Item'),
        t('Qty'),
        t('Price')
      );

      foreach ($order_details->products as $product) {
        $ordered_products[] = array(
          $product->model,
          $product->title,
          $product->qty,
          $product->price
        );
      }

      // Set the order content
      $this->order->setOrderContent('<![CDATA[' . theme('table', $table_headers, $ordered_products) . ']]>');
    }
    else {
      throw new Exception('No order items has been set');
    }

    // ************************* SETTING CARD DETAILS *************************************
    // Set the card type and issue number
    
    if (isset($order_details->payment_details['cc_type']) &&
        (isset($order_details->payment_details['cc_cvv']) || isset($order_details->payment_details['cc_issue']))) {

      // Object creation of Payment Card
      $this->paymentCard = new PaymentCard();
      // Object creation of Payment Card Type
      $payment_card_type = new PaymentCardType();

      // Check whether the gateway supports the given card type. If so get the XML request friendly name of it
      $card_type = trim($order_details->payment_details['cc_type']);
      $supported_card_type = $payment_card_type->isSupportedCardType($card_type);

      // Check if the card type entered is supported by the Gateway
      if ($supported_card_type) {
        $this->paymentCard->setCardType($supported_card_type);

        // Set the issue of cvc/cvv
        if ($card_type == "Solo") {
          $this->paymentCard->setCardIssueNo($order_details->payment_details['cc_issue']);
        }
        else {
          $this->paymentCard->setCardCvc($order_details->payment_details['cc_cvv']);
        }
      }
      else {
        throw new Exception('Card type not supported');
      }
    }
    else {
      throw new Exception('No card type/cvc or issue has been set');
    }

    // Set the credit card number
    if (isset($order_details->payment_details['cc_number'])) {
      $this->paymentCard->setCardNumber(trim($order_details->payment_details['cc_number']));
    }
    else {
      throw new Exception('No card number has been set');
    }

    // Set card holder name
    if (isset($order_details->payment_details['cc_owner'])) {
      $this->paymentCard->setCardHolderName(trim($order_details->payment_details['cc_owner']));
    }
    else {
      throw new Exception('No card holder name has been set');
    }
    
    // Setting the expiry date
    if (isset($order_details->payment_details['cc_exp_month']) && isset($order_details->payment_details['cc_exp_year'])) {
      $this->paymentCard->setExpiryDate(
        trim($order_details->payment_details['cc_exp_month']),
        trim($order_details->payment_details['cc_exp_year'])
      );
    }
    else {
      throw new Exception('No expiry date set has been set');
    }
    
    // Set the billing address
    if (
      isset($order_details->billing_street1) &&
      isset($order_details->billing_postal_code) &&
      isset($order_details->billing_country) &&
      isset($order_details->billing_first_name) &&
      isset($order_details->billing_last_name)
    ) {

      $this->paymentCard->setCardHolderAddress(
        $order_details->billing_first_name,
        $order_details->billing_last_name,
        $order_details->billing_street1,
        $order_details->billing_postal_code,
        $order_details->billing_country
      );
    }
    else {
      throw new Exception('No billing details has been set');
    }

    // Set the billing phone (Optional). We do not throw any exceptions if not set
    if (isset($order_details->billing_phone)) {
      $this->paymentCard->setCardHolderTelephone($order_details->billing_phone);
    }

    // *************************** Shopper Session Details *********************************
    if (
      isset($order_details->host) &&
      isset($order_details->uid) &&
      isset($_SERVER['HTTP_USER_AGENT']) &&
      isset($_SERVER['HTTP_ACCEPT'])
    ) {
      $this->shopperSession = new ShopperSession();
      $this->shopperSession->setShopperIP($order_details->host);
      $this->shopperSession->setUserid($order_details->uid);
      $this->shopperSession->setShopperBrowserUserAgent($_SERVER['HTTP_USER_AGENT']);
      $this->shopperSession->setShopperBrowserAcceptHeader($_SERVER['HTTP_ACCEPT']);
    }
    else {
      throw new Exception('Error occurred during setting the shopper session');
    }

    if (isset($_SERVER['HTTP_USER_AGENT']) && isset($_SERVER['HTTP_ACCEPT'])) {

    }

    //**************************** Shopper Email *******************************************
    if ($order_details->primary_email) {
      $this->shopperEmail = trim($order_details->primary_email);
    }

    // *************************************************************************************
    // Everything went fine. So we return TRUE
    return TRUE;
  }

/**
 * Process the payment
 * @param $merchant_code
 * @param $installation_id
 * @param $service_password
 * @param string $test_mode
 * @return boolean
 */
  public function processPayment($merchant_code, $installation_id, $service_password, $test_mode = 'test') {
    $payment_request_xml = $this->getPaymentRequestXML($merchant_code, $installation_id);

    // Set the service gateway endpoint
    if ($test_mode == 'test') {
      $endpoint_url = PaymentProcess::TEST_GATEWAY_ENDPOINT;
    }
    else {
      $endpoint_url = PaymentProcess::LIVE_GATEWAY_ENDPOINT;
    }

    // Do the curl request to the gateway
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $endpoint_url);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
    curl_setopt($ch, CURLOPT_USERPWD, "$merchant_code:$service_password");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_POST, TRUE) ;
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payment_request_xml);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_TIMEOUT, 240);
    $gateway_response = curl_exec($ch);
    $response_header_code = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);

    // Handling Curl Errors
    if(curl_errno($ch)){
      throw new Exception(curl_error($ch));
    }

    // Close Curl connection
    curl_close($ch);

    // Handling the Gateway response
    if ($response_header_code != 200) {
      throw new Exception("Payment processing request failed. Please try again. HTTP Response code : $response_header_code" );
    }
    else {
      // Get the gateway response object
      $gateway_response_object = simplexml_load_string($gateway_response);

      if ($gateway_response_object) {
        if (
          isset($gateway_response_object->reply->orderStatus->payment->lastEvent) &&
          $gateway_response_object->reply->orderStatus->payment->lastEvent != 'AUTHORISED'
        ) {
          throw new Exception("Payment card was refused by the Gateway");
        }
      }
      else {
        throw new Exception("Invalid gateway response object : $gateway_response");
      }
    }

    return TRUE;
  }

 /**
  * Get the payment request XML
  * @param $merchant_code
  * @param $installation_id
  * @return mixed
  */
  private function getPaymentRequestXML($merchant_code, $installation_id) {
    $xml_string_request_base = <<<XML
<!DOCTYPE paymentService PUBLIC "-//WorldPay/DTD WorldPay PaymentService v1//EN" "http://dtd.worldpay.com//paymentService_v1.dtd">
<paymentService version="1.4" merchantCode="$merchant_code">
</paymentService>
XML;

    // Load the xml as a SimpleXML
    $xml_request = new SimpleXMLElement($xml_string_request_base);

    // Adding the submit method
    $submit = $xml_request->addChild('submit');
    
    // Setting up the order
    $order = $submit->addChild('order');
    $order->addAttribute('orderCode', $this->order->getOrderCode());
    $order->addAttribute('installationId', $installation_id);

    $order->addChild('description', $this->order->getOrderDescription());
    $order_amount = $order->addChild('amount');
    $order_amount->addAttribute('value', $this->order->getAmount());
    $order_amount->addAttribute('currencyCode', $this->order->getCurrencyCode());
    $order_amount->addAttribute('exponent', 2);

    $order->addChild('orderContent', $this->order->getOrderContent());

    // Setting up payment details
    $payment_details = $order->addChild('paymentDetails');
    $payment_details_card_details = $payment_details->addChild($this->paymentCard->getCardType());
    $payment_details_card_details->addChild('cardNumber', $this->paymentCard->getCardNumber());
    $payment_details_card_details_expiry_date = $payment_details_card_details->addChild('expiryDate');
    $payment_details_card_details_expiry_date_fields = $payment_details_card_details_expiry_date->addChild('date');

    $expiry_date = $this->paymentCard->getExpiryDate();
    $payment_details_card_details_expiry_date_fields->addAttribute('month', $expiry_date['month']);
    $payment_details_card_details_expiry_date_fields->addAttribute('year', $expiry_date['year']);

    $payment_details_card_details->addChild('cardHolderName', $this->paymentCard->getCardHolderName());

    // The Solo cards should have a issue number so we set it here. There is no issue number for the other card types
    if (preg_match('/SOLO/', $this->paymentCard->getCardType())) {
      $payment_details_card_details->addChild('issueNumber', $this->paymentCard->getCardIssueNo());
    }
    else {
      $payment_details_card_details->addChild('cvc', $this->paymentCard->getCardCvc());
    }

    $billing_address_details = $this->paymentCard->getCardHolderAddress();

    $payment_details_card_details_card_address = $payment_details_card_details->addChild('cardAddress');
    $payment_details_card_details_address = $payment_details_card_details_card_address->addChild('address');
    $payment_details_card_details_address->addChild('firstName', $billing_address_details['first_name']);
    $payment_details_card_details_address->addChild('lastName', $billing_address_details['last_name']);
    $payment_details_card_details_address->addChild('street', $billing_address_details['street']);
    $payment_details_card_details_address->addChild('postalCode', $billing_address_details['postal_code']);
    $payment_details_card_details_address->addChild('countryCode', $billing_address_details['country_code']);
    $payment_details_card_details_address->addChild('telephoneNumber', $this->paymentCard->getCardHolderTelephone());

    $payment_details_card_details_shopper_session = $payment_details->addChild('session');
    $payment_details_card_details_shopper_session->addAttribute('shopperIPAddress', $this->shopperSession->getShopperIP());
    $payment_details_card_details_shopper_session->addAttribute('id', $this->shopperSession->getUserid());

    // Setting shopper details
    $shopper_details = $order->addChild('shopper');
    $shopper_details->addChild('shopperEmailAddress', $this->shopperEmail);

    // 3D secure details
    $shopper_browser = $shopper_details->addChild('browser');
    $shopper_browser->addChild('acceptHeader', $this->shopperSession->getShopperBrowserAcceptHeader());
    $shopper_browser->addChild('userAgentHeader', $this->shopperSession->getShopperBrowserUserAgent());

    return $xml_request->asXML();
  }
}
