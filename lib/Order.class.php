<?php
/**
 * The class to hold the order details
 */
class Order {
  private $orderCode = '';
  private $orderDescription = '';
  private $currencyCode = '';
  private $amount = 0;
  private $orderContent;

/**
 * Set the order amount
 * @param $amount
 * @return void
 */
  public function setAmount($amount) {
    $this->amount = $amount;
  }

  /**
   * Get the order amount
   * @return float
   */
  public function getAmount() {
    return $this->amount;
  }

  /**
   * Set the currency code
   * @param $currencyCode
   * @return void
   */
  public function setCurrencyCode($currencyCode) {
    $this->currencyCode = $currencyCode;
  }

  /**
   * Get the currency code
   * @return string
   */
  public function getCurrencyCode() {
    return $this->currencyCode;
  }

  /**
   * Set the order code. This is your
   * Ubercart order id
   * @param $orderCode
   * @return void
   */
  public function setOrderCode($orderCode) {
    $this->orderCode = $orderCode;
  }

  /**
   * Get the order code
   * @return string
   */
  public function getOrderCode() {
    return $this->orderCode;
  }

  /**
   * Set the order content
   * @param $orderContent
   * @return void
   */
  public function setOrderContent($orderContent) {
    $this->orderContent = $orderContent;
  }

  /**
   * Get the order content
   * @return string
   */
  public function getOrderContent() {
    return $this->orderContent;
  }

  /**
   * Set the order description
   * @param $orderDescription
   * @return void
   */
  public function setOrderDescription($orderDescription) {
    $this->orderDescription = $orderDescription;
  }

  /**
   * get the order description
   * @return string
   */
  public function getOrderDescription() {
    return $this->orderDescription;
  }
}
