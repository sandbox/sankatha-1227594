<?php
/**
 * The Worldpay gateway Admin settings form
 * @param $form
 * @param $form_state
 * @param $form_id
 * @return void
 */
function uc_worldpay_xml_direct_form_alter(&$form, $form_state, $form_id) {
  switch ($form_id) {
    case 'uc_payment_gateways_form':
      // Loop through each of the gateways on the form.
      foreach (element_children($form['gateways']) as $gateway) {
        if ($gateway == 'uc_worldpay_xml_direct_payment_gateway') {
          // Add the Worldpay gateway settings form elements
          $form['gateways'][$gateway]['uc_worldpay_xml_direct_fieldset_wrapper'] = array(
            '#type' => 'fieldset',
            '#title' => t('Worldpay XML Direct gateway installation settings'),
            '#collapsible' => TRUE,
            '#collapsed' => FALSE
          );

          $form['gateways'][$gateway]['uc_worldpay_xml_direct_fieldset_wrapper']['uc_worldpay_xml_direct_gateway_to_use'] = array(
            '#type' => 'select',
            '#options' => array(
              'test' => t('Test'),
              'live' => t('Live')
            ),
            '#title' => t('Gateway to use'),
            '#description' => t('Use the test gateway if you are testing your installation and switch to live if everything is fine.'),
            '#default_value' => variable_get('uc_worldpay_xml_direct_gateway_to_use', 'test'),
            '#required' => TRUE
          );
          
          $form['gateways'][$gateway]['uc_worldpay_xml_direct_fieldset_wrapper']['uc_worldpay_xml_direct_merchant_code'] = array(
            '#type' => 'textfield',
            '#title' => t('Merchant Code'),
            '#description' => t('Your Worldpay Gateway merchant code'),
            '#default_value' => variable_get('uc_worldpay_xml_direct_merchant_code', ''),
            '#required' => TRUE
          );
          
          $form['gateways'][$gateway]['uc_worldpay_xml_direct_fieldset_wrapper']['uc_worldpay_xml_direct_installation_id'] = array(
            '#type' => 'textfield',
            '#title' => t('Installation ID'),
            '#description' => t('Your Worldpay Gateway installation id'),
            '#default_value' => variable_get('uc_worldpay_xml_direct_installation_id', ''),
            '#required' => TRUE
          );

          $form['gateways'][$gateway]['uc_worldpay_xml_direct_fieldset_wrapper']['uc_worldpay_xml_direct_password'] = array(
            '#type' => 'textfield',
            '#title' => t('Password'),
            '#description' => t('Your Worldpay Gateway password'),
            '#default_value' => variable_get('uc_worldpay_xml_direct_password', ''),
            '#required' => TRUE
          );

          break;
        }
      }
     break;
  }
}


 